import requests
import os
import sys
from datetime import datetime, timedelta
from uuid import uuid4
from enum import Enum


class ScanType(str, Enum):
    SARIF = "SARIF" # .sarif
    GITLEAKS = "Gitleaks Scan" # .json
    SEMGREP = "Semgrep JSON Report" # .jsono
    BANDIT = "Bandit Scan" # .json
    RETIRE = "Retire.js Scan" # .json
    DEPENDENCY_CHECK = "Dependency Check Scan" # .xml
    TRIVY = "Trivy Scan" # .json


DD_TOKEN = os.environ.get("DD_TOKEN")
DD_URL = os.environ.get("DD_URL", "https://demo.defectdojo.org/")
DD_PRODUCT_ID = int(os.environ.get("DD_PRODUCT_ID", 1))

CI_COMMIT_MESSAGE = os.environ.get("CI_COMMIT_MESSAGE")
CI_COMMIT_DESCRIPTION = os.environ.get("CI_COMMIT_DESCRIPTION")
CI_COMMIT_SHORT_SHA = os.environ.get("CI_COMMIT_SHORT_SHA")
CI_PROJECT_URL = os.environ.get("CI_PROJECT_URL")

if CI_COMMIT_DESCRIPTION is None or CI_COMMIT_DESCRIPTION == "":
    CI_COMMIT_DESCRIPTION = uuid4().hex

filename_ = sys.argv[1]

if DD_URL[-1] == "/":
    DD_URL = DD_URL[:-1]

url = f'{DD_URL}/api/v2/engagements/'
headers = {
    'Authorization': f'Token {DD_TOKEN}'
}

engag_resp = requests.request("GET", url, headers=headers, params={"name": CI_COMMIT_MESSAGE})

if not engag_resp.json().get("results"):
    payload = {
        "name": CI_COMMIT_MESSAGE,
        "description": CI_COMMIT_DESCRIPTION,
        "version": "0.1.0",
        "target_start": datetime.now().strftime(r"%Y-%m-%d"),
        "target_end": (datetime.now() + timedelta(30)).strftime(r"%Y-%m-%d"),
        "status": "In Progress",
        "engagement_type": "CI/CD",
        "commit_hash": CI_COMMIT_SHORT_SHA,
        "source_code_management_uri": CI_PROJECT_URL,
        "lead": 1,
        "product": DD_PRODUCT_ID,
    }

    r = requests.request("POST", url, headers=headers, json=payload)
    engag_resp = r.json()

else:
    engag_resp = sorted(engag_resp.json()["results"])[-1]


data = {
    'active': True,
    'verified': True,
    'minimum_severity': 'Low',
    'commit_hash': CI_COMMIT_SHORT_SHA,
    'scan_type': '',
    'source_code_management_uri': CI_PROJECT_URL,
    'engagement': engag_resp["id"],
}

if "semgrep" in filename_.lower():
    scan_type_ = ScanType.SEMGREP
elif "gitleaks" in filename_.lower():
    scan_type_ = ScanType.GITLEAKS
elif "retire" in filename_.lower():
    scan_type_ = ScanType.RETIRE
elif "bandit" in filename_.lower():
    scan_type_ = ScanType.BANDIT
elif "dependency-check-report" in filename_.lower():
    # dependency-check-report.xml
    scan_type_ = ScanType.DEPENDENCY_CHECK
elif "trivy" in filename_.lower():
    scan_type_ = ScanType.TRIVY
elif "sarif" in filename_.lower():
    scan_type_ = ScanType.SARIF
else:
    raise Exception("Invalid scan_type, check DefectDojo documents.")

data["scan_type"] = scan_type_.value

file = {
    'file': open(filename_, 'rb')
}

response = requests.post(f'{DD_URL}/api/v2/import-scan/', headers=headers, data=data, files=file)

print(response.status_code)
print(response.json())
